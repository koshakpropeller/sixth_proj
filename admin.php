<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.

function delete_user($id) {
    $user = 'u20386';
    $pass = '2551027';
    $db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
try {
    $stmt = $db->prepare("DELETE FROM app WHERE id=" . $id);

    $stmt->execute();
}
catch(Exception $e) {
    print('Error : ' . $e->getMessage());
    exit();
}
}
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ) {


  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}
else {
    $login = $_SERVER['PHP_AUTH_USER'];
    $password =  md5($_SERVER['PHP_AUTH_PW']);
    $user = 'u20386';
    $pass = '2551027';
    $db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    // Если все ок, то авторизуем пользователя.
    try {
        $stmt = $db->prepare("SELECT id uid FROM admin WHERE  login=:login AND pass=:pass");

        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->bindParam(':pass', $password, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();

        if (empty($data)) {
            header('HTTP/1.1 401 Unanthorized');
            header('WWW-Authenticate: Basic realm="My site"');
            print('<h1>401 Требуется авторизация</h1>');
            exit();
        }
    }
    catch(Exception $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
}
$user = 'u20386';
$pass = '2551027';
$db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
try {
    $stmt = $db->prepare("SELECT id, name, email, year, gender, lungs, nodeath, through, levitation, biography FROM app ");
    $stmt->execute();
    $data = $stmt->fetchAll();

}
catch(Exception $e) {
    print('Error : ' . $e->getMessage());
    exit();
}
if(!empty($_GET['id'])) {
    delete_user($_GET['id']);
}
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
?>
 <!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Админ</title>
    </head>
    <body>
    <table class="table">
        <thead>
           <tr>
               <th scole="col">#</th>
               <th scope="col">name</th>
               <th scope="col">email</th>
               <th scope="col">year</th>
               <th scope="col">gender</th>
               <th scope="col">lungs</th>
               <th scope="col">nodeath</th>
               <th scope="col">through</th>
               <th scope="col">levitation</th>
               <th scope="col">biography</th>
               <th scope="col"></th>
           </tr>
        </thead>
        <tbody>
    <?php
       foreach ($data as &$row) {
           print("<tr>");
           print("<td>" . $row['id'] . "</td>");
           print("<td>" . $row['name'] ."</td>");
           print("<td>" . $row['email'] ."</td>");
           print("<td>" . $row['year'] ."</td>");
           print("<td>" . ($row['gender'] == 0 ? "female" : "male") ."</td>");
           print("<td>" . ($row['lungs'] == 0 ? "0" : ">0")."</td>");
           print("<td>" . ($row['nodeath']  == 1 ? "+" : "-")."</td>");
           print("<td>" . ($row['through'] == 1 ? "+" : "-")."</td>");
           print("<td>" . ($row['levitation'] == 1 ? "+" : "-")."</td>");
           print("<td>" . $row['biography'] ."</td>");
           $str = "./admin.php?id=" . $row['id'];
           print("<td>" . '<a class="btn btn-danger"  href="' . $str . '">delete</a>' . "</td>");
           print("</tr>");
       }
    ?>
        </tbody>
    </table>
    </body>
 </html>
